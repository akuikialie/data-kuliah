#include <stdio.h>
#include <math.h>
#define MAX 100

typedef struct{
    int data[MAX];
    int front;
    int rear;
    int count;
} Queue ;

Queue antri;
Queue *Q;

void Inisialisasi(){
    Q = &antri;
    Q->front = -1;
    Q->rear = -1;
    Q->count = 0;
}

int isEmpty()
{ 
    if (Q->front == -1) {
        printf("Antrian sedang kosong\n");
        return 1;
    } else {
        return 0;
    }
}

int isFull()
{
    if (Q->rear == MAX - 1) {
        printf("Antrian sedang penuh\n");
        return 1;
    } else {
        return 0;
    }
}

void geser(int shift)
{
    int bil;
        
    for (int i = 1; i <= shift; i++) {
        if (isEmpty() == 1) {
            break;
        }

        bil = Q->data[Q->front];
        int tmp1 = 0, tmp2 = 0;
        for (int j = (Q->count - 1); j >= Q->front; j-- )
        {
                
            if (j > (Q->rear - 1)) {
                tmp1 = Q->data[j];
                Q->data[j] = bil;
            } else {
                tmp2 = Q->data[j];
                Q->data[j] = tmp1;
                tmp1 = tmp2;
            }
        }           
    }
}

void convertDesimalToBinary(int desimal)
{
    if (desimal > 0 ){
        int sisa, bagi, bil;
        bil = desimal;
            
        do{
            sisa = bil % 2; 
            bagi = bil / 2;
            bil = bagi;
            
            if(isFull() == 1){
                break;
            }
            
            Q->rear++;
            Q->data[Q->rear] = sisa;
            Q->count++;

            if(Q->front == -1) Q->front = 0;
        }while(bil > 0);
    }
}


double convertBinaryToDesimal()
{
    double hasil = 0.0;
    for (int i = 0 ;i <= (Q->count - 1); i++ ) {
        hasil += (pow(2,i) * Q->data[i]);
    }
    return hasil;
}

void viewData()
{
    for (int i = Q->rear; i >= Q->front; i-- ) {
        printf("%d",Q->data[i]);
    }
    printf("\n");
}


void main(){
    int desimal, shift;
    double hasil;

    Inisialisasi();

    printf("Masukkan bilangan desimal : ");
    scanf("%d", &desimal);

    printf("Masukkan jumlah shift : ");
    scanf("%d", &shift);
    convertDesimalToBinary(desimal);

    geser(shift);

    hasil = convertBinaryToDesimal();

    printf("Bilangan desimal setelah shift : %.0f\n", hasil);
}

