#include<stdio.h>
#define N 5
#define M 1000
#define MAX 20

typedef int typeItem;
typedef struct {
	typeItem item[MAX];
	int front, rear, count;
} Queue;

void Enqueue(Queue *q,typeItem x) {
	if(Penuh(q))
		printf("Queue Penuh\n"); 
	else {
		q->item[q->rear] = x;
		q->rear = (q->rear +1) %MAX;
		q->count++;
	}
}

typeItem Dequeue(Queue *q)
{
	typeItem temp;
	if(Kosong(q)) printf("Queue kosong\n");
	else {
		temp = q->item[q->front];
		q->front = (q->front + 1);
		q->count--;
	}
}

int Kosong(Queue *q) {
	return q->count == 0;
}
int Penuh(Queue *q) {
	return q->count == MAX;
}

void inisialisasiQueue(Queue *q)
{
	q->front = q->rear = q->count=0;
}



void cetak(int arr[], char judul[]) {
	int i;
	printf("%s: ", judul);
	for(i = 0;i<N;i++)
		printf("%d ", arr[i]);
	printf("\n");
}
int ada_di_queue(Queue *q, int i)
{
	int p = q->front, ada = 0;
	while(p!=q->rear) {
		if(q->item[q->front] == i) ada = 1;
		p = (p+1) %MAX;
	}
}

void inisialisasi(int Q[], int R[], int awal)
{
	int i;
	for(i=0;i<N;i++){
		if(i == awal) {
			Q[i] = 0;
		} else {
			Q[i] = M;
			R[i] = 0;
		}
	}
}
main() {
	int input[N][N] = {M,1,3,M,M, 
		M,M,1,M,5, 
		3,M,M,2,M, 
		M,M,M,M,1, 
		M,M,M,M,M
	};

	int Q[N], R[N];
	int awal ,akhir, CR, i;

	Queue t;
	inisialisasiQueue(&t);
	printf("Titik awal : ");
	scanf("%d", &awal);
	printf("Titik akhir : ");
	scanf("%d", &akhir);

	inisialisasi(Q, R, awal);

	cetak(Q, "Beban");
	cetak(R, "Rute");

	Enqueue(&t, awal);
	
	while(!Kosong(&t)) {
		CR = Dequeue(&t)-1;
		for(i = 0;i<N;i++)
			if(input[CR][i] != M) {
				if(Q[CR] + input[CR][i]< Q[i]) {
					Q[i] = Q[CR] + input[CR][i];
					R[i] = CR +1;
					if(i+1!=awal && i+1!=akhir && !ada_di_queue(&t, i+1)) Enqueue(&t, i+1);
				}

			}

	}

	cetak(Q, "Beban");
	cetak(R, "Rute");

}