#include <stdio.h>
#include <stdlib.h>

typedef struct Node Tree;
typedef char typeitem;
struct Node {	
	typeitem item;
	Tree *kiri;
	Tree *kanan;
};

Tree *baru, *root = NULL, *P, *Q;

void alokasi_node(typeitem x) {
	baru=(Tree *)malloc(sizeof(Tree)); 
	if (baru == NULL) {
		printf("Alokasi gagal");exit(1);
	} else {
		baru->item=x; 
		baru->kiri=NULL; 
		baru->kanan=NULL; 
	} 
} 
void preorder(Tree *root) 
{
	if(root != NULL) {
		printf("%c", root->item);
		preorder(root->kiri);
		preorder(root->kanan);
	}
}

void inorder(Tree *root) 
{
	if(root != NULL) {
		inorder(root->kiri);
		printf("%c", root->item);
		inorder(root->kanan);
	}
}


void postorder(Tree *root) 
{
	if(root != NULL) {
		postorder(root->kiri);
		postorder(root->kanan);
		printf("%c", root->item);
	}
}

void main(){

	char kar, lagi = 'y';

	while(lagi == 'y') {
		fflush(stdin);
		printf("masukkan karakter : ");
		scanf("%c", &kar);
		alokasi_node(kar);

		if(root == NULL) {
			root = baru;
		} else {
			P = Q = root;
			while(Q!=NULL && baru->item != P->item) {
				P = Q;
				if (baru->item < P->item) {
					Q = P->kiri;
				} else {
					Q = P->kanan;
				}
			}
			if(baru->item == P->item) {
				printf("Data e Sama");
			} else {
				if(baru->item < P->item) {
					P->kiri = baru;
				} else {
					P->kanan = baru;
				}
			}
		}
		fflush(stdin);
		printf("Ulangi (y/n): ");
		scanf("%c", &lagi);
	}
	printf("\n+++ Pre Order +++\n");
	preorder(root);

	printf("\n+++ In Order +++\n");
	inorder(root);

	printf("\n+++ Post Order +++\n");
	postorder(root);
	printf("\n");
}
