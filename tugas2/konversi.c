#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAXSTACK 100 
typedef int ItemType;

/* Definisi struktur stack */
typedef struct {
    int Item[MAXSTACK]; /* Array yang berisi datatumpukan 
*/
    int Count; /* menunjukkan indeks data paling atas dari 
stack */
} Stack;

Stack coba;
int i, x, sisabagi; 

void InitializeStack(Stack *S) {
    S->Count = 0;
}

int Empty(Stack *S) {
    return (S->Count == 0);
}

int Full(Stack *S) {
    return (S->Count == MAXSTACK);
}

void Push(ItemType x, Stack *S) {
    if (S->Count == MAXSTACK) {
        printf("Stack penuh! Data tidak dapat masuk!");
    } else {
        S->Item[S->Count] = x;
        ++(S->Count);
    }
}

int Pop(Stack *S) {
    int x;

    if (S->Count == 0) {
        printf("Stack masih kosong!");
        return 0;
    } else {
        --(S->Count);
        x = S->Item[S->Count];
        return x;
    }
}
void konversiBiner(int input) {
    while (input != 0) {
        sisabagi = input % 2;
        Push(sisabagi, &coba);
        input = (input - sisabagi) / 2;
    }

    for (i = coba.Count; i > 0; i--) {
        x = Pop(&coba);
        printf("%d", x);
    }
    printf("\n");
}

void konversiOktal(int input) {
    while (input != 0) {
        sisabagi = input % 8;
        Push(sisabagi, &coba);
        input = (input - sisabagi) / 8;
    }

    for (i = coba.Count; i > 0; i--) {
        x = Pop(&coba);
        printf("%d", x);
    }
    printf("\n");
}

void konversiHeksa(int input) {
    while (input != 0) {
        sisabagi = input % 16;
        Push(sisabagi, &coba);
        input = (input - sisabagi) / 16;
    }

    for (i = coba.Count; i > 0; i--) {
        x = Pop(&coba);
        printf("%d", x);
    }
    printf("\n");
}

void konversiAll(int input) 
{
    printf("Bilangan Biner = ");
    konversiBiner(input);
    printf("Bilangan Oktal = ");
    konversiOktal(input);
    printf("Bilangan Heksa = ");
    konversiHeksa(input);
}

void main() {

    int decimal, pil;
    char ulangi;

    InitializeStack(&coba);

    printf("Bilangan Decimal : ");
    scanf("%d", &decimal);
    if (decimal > 0) {
        konversiAll(decimal);
    }

    printf("\n\n");
}
