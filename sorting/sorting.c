#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#define N 5


void insertition(int data[]) {
	int i, j, key;
	i = 1;
	while(i<N) {
		key = data[i];
		j = i-1;
		while(j>=0 && data[j]>key) {
			data[j+1] = data[j];
			j--;
		}
		data[j+1] = key;
		i++;
	}
}

/*void selection(int data[]) {
	int i, j, min, min_id, temp;

	i = 1;
	while(i<N-1) {
		min = data[i];
		min_id = i;
		j= i+1;
		while(j<N) {
			if(data[j] < min) {
				min= data[j];
				min_id =j;
			}
			j++;
		}
		temp = data(i);
		data[i]= data[min_id];
		data[min_id]=temp;
		i++;
	}
}*/

void bubble(int data[]) {
	int i, j, temp;
	for (int i = 0; i < N-1; i++)
	{
		for (int j = 0; j < N-1-i; j++)
		{
			if(data[j]>data[j+1]){
				temp = data[j];
				data[j] = data[j+1];
				data[j+1] = temp;
			}
		}
	}
}

void bubbleflag(int data[]) {
	int i, j, temp, did_swap = 1;
	i=0;
	while(i<N && did_swap){
		did_swap = 0;
		for (int j = 0; j < N-1-i; j++)
		{
			if(data[j]>data[j+1]){
				temp = data[j];
				data[j] = data[j+1];
				data[j+1] = temp;
				did_swap = 1;
			}
			i++;
		}
	}
}

void shell(int data[]) {
	int i, jarak, temp, did_swap = 1;
	jarak = N;
	while(jarak >= 1) {
		jarak = jarak/2;
		did_swap = 1;
		while(did_swap) {
			i = 0;
			did_swap = 0;
			while (i<N - jarak) {
				if(data[i]>data[i+jarak]) {
					temp = data[i];
					data[i] = data[i+jarak];
					data[i+jarak] = temp;
					did_swap = 1;
				}
				i++;
			}

		}
	}
}

/*void Quicksort(int data[], int p, int r)
{
	int q;
	if (p < r)
	{
		q = partition(data, p, r);
		Quicksort(data, p, q);
		Quicksort(data, q+1, r);
	}
}*/

int partition(int data[], int p, int r) {
	int x, i, j, temp;
	x = data[p];
	i = p ; 
	j = r ;
	while(i<j){
		while(data[j] > x){
			j--;
		}
		while(data[i] < x){
			i++;
		}
		if (i < j){
			temp = data[i];
			data[i] = data[j];
			data[j] = temp;
			j--;
			i++;
		}
		else {
			return j;
		}
	}

}

void MergeSortRekursif(int data[],int l, int r) {
	int med;
	if (l < r) {
		med = (l+r) / 2 ;
		MergeSortRekursif(data, l,med);
		MergeSortRekursif(data, med+1,r);
		MergeSort(data, l,med,r);
	}
}

void MergeSort(int data[], int left, int median, int right) {
	int kiri1, kiri2, kanan1, kanan2, hasil[N], i, j;
	kiri1 = left;
	kanan1 = median;
	kiri2 = median+1;
	kanan2 = right;
	i = left;

    while ((kiri1<=kanan1) && (kiri2<=kanan2)){
		if (data[kiri1] <= data[kiri2]) {
			hasil[i] = data[kiri1];
			kiri1++;
		}else {
			hasil[i] = data[kiri2];
			kiri2++;			
		}
		i++;
	}
	while (kiri1<=kanan1) {
		hasil[i] = data[kiri1];
		kiri1++;
		i++;
	}
	while (kiri2<=kanan2) {
		hasil[i] = data[kiri2];
		i++;
	 	kiri2++;
	}
	j=left;
	while (j <=right){
		data[j] = hasil[j];
		j++;
	}
}

void main () {
	int A[N], i, data[N], pilih;
	char lagi = 'y';

	for (i = 0; i < N; i++)
	{
		srand(time(NULL)*(i+1));
		A[i] = rand()%100;
		printf(" %d ",A[i]);
	}

	while (lagi == 'y') {
		for (int a = 0; a < N; ++a)
		{
			data[a] = A[a];
		}
		printf("\n");
		printf("1. Insertition \n");
		printf("2. Selection \n");
		printf("3. Bubble \n");
		printf("4. Bubble Flag \n");
		printf("5. Shell \n");
		printf("6. Quicksort \n");
		printf("7. Merge \n");
		printf("Pilih : ");
		scanf("%d", &pilih);

		switch(pilih) {
			case 1:
				insertition(data);
			break;
			// case 2:
			// 	selection(data);
			// break;
			case 3:
				bubble(data);
			break;
			case 4:
				bubbleflag(data);
			break;
			case 5:
				shell(data);
			break;
			// case 6:
			// 	Quicksort(data, 0, N-1);
			// break;
			case 7:
				MergeSortRekursif(data, 0, N-1);
			break;
		}
		printf("data urut : ");
		for(i=0;i<N;i++) {
			printf(" %d ", data[i]);
		}
		printf("\nlagi?");
		scanf("%c", &lagi);
	}
}