#include<stdio.h>
#include<stdlib.h>

typedef struct simpul Node;
struct simpul{
	int data;
	Node *next;
};

Node *head=NULL, *baru;

void allocate_node(int x)
{
	baru = (Node *)malloc(sizeof(Node));
	if(baru==NULL) {
		printf("Alokasi gagal\n");
		exit(1);
	} else {
		baru->data=x;
		baru->next=NULL;
	}
}

void tampil()
{
	Node *t = head;
	while(t!=NULL) {
		printf("%d ", t->data);
		t=t->next;
	}
	printf("\n");
}

void sisip_awal()
{
	baru->next = head;
	head=baru;
}

void sisip_akhir()
{
	Node *akhir = head;
	while(akhir->next!=NULL)
		akhir = akhir->next;
	akhir->next = baru;
}

void sisip_sebelum(int x)
{
	Node *before = head;
	while(before->next->data!=x){
		before=before->next;
	}
	baru->next = before->next;
	before->next = baru;
}

void sisip_setelah(int x)
{
	Node *after = head;
	while(after->data!=x){
		after=after->next;
	}
	baru->next = after->next;
	after->next = baru;
}

void free_node(Node *p) {
	free(p);
	p = NULL;
}

void hapus_awal() {
	Node *hapus = head;
	head=hapus->next;
	free_node(hapus);
}

void hapus_akhir() {
	Node *akhir, *hapus;
	akhir=hapus=head;
	while(hapus->next!=NULL) {
		akhir = hapus;
		hapus=hapus->next;
	}
	akhir->next=NULL;
	free_node(hapus);
}

void hapus_tengah(int x) {
	Node *hapus, *before;
	hapus = before=head;
	while(hapus->data !=x) {
		before=hapus;
		hapus=hapus->next;
	}
	before->next=hapus->next;
	free_node(hapus);
}

void main() 
{
	char lagi='y';
	int pilih, x, sisip;

	allocate_node(10);
	head= baru;
	tampil();

	while (lagi=='y') {
		printf("1. Sisip Awal\n");
		printf("2. Sisip Akhir\n");
		printf("3. Sisip sebelum Simpul\n");
		printf("4. Sisip setelah Simpul\n");
		printf("5. Hapus Simpul tertentu\n");
		printf("6. Hapus Awal\n");
		printf("7. Hapus akhir\n");
		printf("Pilih : ");
		scanf("%d", &pilih);

		switch(pilih) {
			case 1:
				printf("Masukkan Data : ");
				scanf("%d", &x);
				allocate_node(x);
				sisip_awal();
			break;
			case 2:
				printf("Masukkan Data : ");
				scanf("%d", &x);
				allocate_node(x);
				sisip_akhir();
			break;
			case 3:
				printf("Masukkan Data : ");
				scanf("%d", &x);
				printf("Masukkan sebelum simpul :");
				scanf("%d", &sisip);
				allocate_node(x);
				sisip_sebelum(sisip);
			break;
			case 4:
				printf("Masukkan Data : ");
				scanf("%d", &x);
				printf("Masukkan setelah simpul :");
				scanf("%d", &sisip);
				allocate_node(x);
				sisip_setelah(sisip);
			break;
			case 5:
				printf("Masukkan simpul :");
				scanf("%d", &x);
				allocate_node(x);
				hapus_tengah(x);
			break;
			case 6:hapus_awal();
			break;
			case 7: hapus_akhir();
			break;
		}

		tampil();
		fflush(stdin);
		printf("Apakah anda ingin lagi?");
		scanf("%c",&lagi);

	}
}

