#include<stdio.h>
#include<stdlib.h>

typedef struct simpul Node;
struct simpul{
	int data;
	Node *next;
};

Node *head=NULL, *baru;

void allocate_node(int x)
{
	baru = (Node *)malloc(sizeof(Node));
	if(baru==NULL) {
		printf("Alokasi gagal\n");
		exit(1);
	} else {
		baru->data=x;
		baru->next=NULL;
	}
}

void tampil()
{
	Node *t = head;
	while(t!=NULL) {
		printf("%d ", t->data);
		t=t->next;
	}
	printf("\n");
}

void sisip() 
{
	Node *p;
	if(head == NULL) {
	
	} else if (head->data > baru->data) {
		sisip_awal();
	} else {
		p=head;
		while(p!=NULL && p->data < baru->data) {
		    p = p->next;

			if(p == NULL) {
				sisip_akhir();
			} else if(p->data == baru->data) {
				printf("Data duplikat");	
			} else {
				sisip_sebelum(p->data);
			}
		}
	}
}

void sisip_awal()
{
	baru->next = head;
	head=baru;
}

void sisip_akhir()
{
	Node *akhir = head;
	while(akhir->next!=NULL)
		akhir = akhir->next;
	akhir->next = baru;
}
void free_node(Node *p) {
	free(p);
	p = NULL;
}

void hapus (int x) {
	Node *hapus =head;

	if(head->data=x) {
		hapus_awal();
	} else 
		while (hapus!=NULL && hapus->data!=x) {
			hapus=hapus->next;
			if(hapus == NULL) {
				printf("data tidak ada yang di hapus\n");
			} else {
				hapus_tengah(hapus->data);
			}
		}		
	}
}

void hapus_awal() {
	Node *hapus = head;
	head=hapus->next;
	free_node(hapus);
}

void hapus_tengah(int x) {
	Node *hapus, *before;
	hapus = before=head;
	while(hapus->data !=x) {
		before=hapus;
		hapus=hapus->next;
	}
	before->next=hapus->next;
	free_node(hapus);
}

void main() 
{
	char lagi='y';
	int pilih, data;

	while (lagi=='y') {
		printf("1. Sisip \n");
		printf("2. Sisip Akhir\n");
		printf("Pilih : ");
		scanf("%d", &pilih);

		switch(pilih) {
			case 1:
				printf("Masukkan Data : ");
				scanf("%d", &data);
				allocate_node(data);
				sisip();
			break;
			case 2:
				printf("Masukkan Data : ");
				scanf("%d", &data);
				allocate_node(data);
				hapus();
			break;
		}

		tampil();
		fflush(stdin);
		printf("Apakah anda ingin lagi?");
		scanf("%c",&lagi);

	}
}

