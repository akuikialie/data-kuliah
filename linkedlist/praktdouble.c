#include <stdio.h>
#include <stdlib.h>

typedef struct simpul Dnode;
struct simpul {
	int data;
	Dnode *prev;
	Dnode *next;
};

Dnode *head=NULL, *tail=NULL, *baru;

void allocate_Dnode(int x) 
{
	baru = (Dnode *)malloc(sizeof(Dnode));

	if(baru == NULL) {
		printf("Alokasi gagal\n");
		exit(1);
	} else {
		baru->data=x;
		baru->next=NULL;
		baru->prev=NULL;
	}
}


void tampil() {
	Dnode *t = head;
	while(t->next!=NULL){
		printf("%d ", t->data);
		t = t->next;
	}
	printf("\n");
	t = tail;
	while(t->prev!=NULL){
		printf("%d ", t->data);
		t = t->prev;
	}
	printf("\n");
	
}

void sisip_awal() 
{
	baru->next = head;
	head->prev = baru;
	head= baru;
}

void sisip_akhir() 
{
	baru->prev = tail;
	tail->next = baru;
	tail=baru;
}

void sisip_sebelum(int x) 
{
	Dnode *before = data;
	while(before->data!=x) {
		before= before->next;
	}
	baru->next = before;
	baru->before = before->prev;
	before->prev->next = baru;
	before->prev=baru;
}

void sisip_setelah(int x) 
{
	Dnode *after=head;
	while(aftar->data!=x) {
		after= after->next;
	}

	baru->next= after->next;
	baru->prev=after;
	after->next->prev = baru;
	after->next = baru;
}

void free_dnode(Dnode *p) {
	free(p);
	p = NULL;
}

void hapus awal () {
	Dnode *hapus =head;
	head= hapus->next;
	head->prev= null;
	free_dnode(hapus);
}

void hapus akhir() {
	Dnode *hapus =tail;
	tail= hapus->next;
	tail->prev= null;
	free_dnode(hapus);
}

void hapus_data(int x) {
	Dnode *hapus = head;
	while(hapus->data!=x) {
		hapus = hapus->next;
	}
	hapus->prev->next=hapus->next;
	hapus->next->prev=hapus->prev;
	free_dnode(hapus);
}