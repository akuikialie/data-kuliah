#include <stdio.h>
#include <math.h>

typedef struct {
    float real;
    float im;
} K;

K tambah (K k1, K k2) 
{
    K hasil;
    hasil.real=k1.real+k2.real;
    hasil.im = k1.im+k2.im;

    return hasil;
}

K kurang (K k1, K k2) 
{
    K hasil;
    hasil.real=k1.real-k2.real;
    hasil.im = k1.im-k2.im;

    return hasil;
}
K kali (K k1, K k2) 
{
    K hasil;
    hasil.real = ((k1.real*k2.real) - (k1.im*k2.im));
    hasil.im = ((k1.real*k2.im)+(k1.im*k2.real));
  
    return hasil; 
}
K bagi (K k1, K k2) 
{
    K hasil;
    hasil.real = ((k1.real*k2.real)+(k2.im*k2.im))/(pow(k1.real, 2)+pow(k2.real, 2));
    hasil.im = 100;
    return hasil;
}

void main() 
{
    K k1, k2, hasilTambah, hasilKurang, hasilKali, hasilBagi;
    printf("masukkan bil 1 (real, im) : \n");
    scanf("%f %f", &k1.real, &k1.im);
    printf("masukkan bil 2 (real, im) : \n");
    scanf("%f %f", &k2.real, &k2.im);

    hasilTambah = tambah(k1, k2);
    printf("Hasil = %5.2f + %5.2f\n", hasilTambah.real, hasilTambah.im);
    printf("\n");
    hasilKurang = kurang(k1, k2);
    printf("Hasil = %5.2f - %5.2f\n", hasilKurang.real, hasilKurang.im);
    printf("\n");
    hasilKali = kali(k1, k2);
    printf("Hasil = %5.2f * %5.2f\n", hasilKali.real, hasilKali.im);
    printf("\n");
    hasilKali = bagi(k1, k2);
    printf("Hasil = %5.2f / %5.2f\n", hasilBagi.real, hasilBagi.im);

}
