#include <stdio.h>
#define ORDO 2

void tambah(int M1[ORDO][ORDO], int M2[ORDO][ORDO]) {
	int i;
	for (i = 0 ; i < ORDO ; i++) {
        int j;
        for (j = 0; j < ORDO ; j++) {
			printf(" %d ", M1[i][j]+ M2[i][j]);
        }
        printf("\n");
    } 
}

void kurang(int M1[ORDO][ORDO], int M2[ORDO][ORDO]) {
	int i;
	for (i = 0 ; i < ORDO ; i++) {
        int j;
        for (j = 0; j < ORDO ; j++) {
			printf(" %d ", M1[i][j] - M2[i][j]);
        }
        printf("\n");
    } 
}

void kali(int M1[ORDO][ORDO], int M2[ORDO][ORDO]) {
	int i;
	for (i = 0 ; i < ORDO ; i++) {
        int j;
        for (j = 0; j < ORDO ; j++) {
			printf(" %d ", M1[i][j] * M2[i][j]);
        }
        printf("\n");
    } 
}

main() 
{
	int mat1[2][2], mat2[2][2], i;
    mat1[0][0] = 10;
    mat1[0][1] = 6;
    mat1[1][0] = 8;
    mat1[1][1] = 4;
    mat2[0][0] = 5;
    mat2[0][1] = 2;
    mat2[1][0] = 1;
    mat2[1][1] = 3;

	printf("Bismillah\n\n");
	
	printf("Matrik 1 : \n");
	for (i = 0 ; i < ORDO ; i++) {
        int j;
        for (j = 0; j < ORDO ; j++) {
			printf(" %d ", mat1[i][j]);
        }
        printf("\n");
    }

	printf("Matrik 2 : \n");
	for (i = 0 ; i < ORDO ; i++) {
        int j;
        for (j = 0; j < ORDO ; j++) {
			printf(" %d ", mat2[i][j]);
        }
        printf("\n");
    }
	
	//Fungsi Operasi Penjumlahan Matrik
	printf("\nHasil Operasi Penjumlahan Matrik\n");
	tambah(mat1, mat2);

	//Fungsi Operasi Pengurangan Matrik
	printf("\nHasil Operasi Penjumlahan Matrik\n");
	kurang(mat1, mat2);

	//Fungsi Operasi Perkalian Matrik
	printf("\nHasil Operasi Penjumlahan Matrik\n");
	kali(mat1, mat2);

}