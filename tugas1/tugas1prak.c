#include <stdio.h>
#include <math.h>
#define N 10

float tambah(int P1[] , int P2[], float x) 
{
    float hasil = 0.0;
    int i;
    for(i = 0; i < N; i++) {
	hasil = hasil + ((P1[i]+P2[i])*(float)pow(x,i));
    }     
    return hasil;
}
float kurang(int P1[], int P2[], float x)
{
    float hasil = 0.0;
    int i;
    for (i = 0; i < N; i++) {
	hasil = hasil + ((P1[i]-P2[i])*(float)pow(x, i));
    }
    return hasil;
}
float kali(int P1[], int P2[], float x)
{
    float hasil = 0.0;
    int i;
    for (i = 0; i < N; i++) {
	hasil = hasil + ((P1[i]*P2[i])*(float)pow(x, i));
    }
    return hasil;
}

void main() 
{
    int P1[N] = { 25, 0, 0, 1, 0, 5, 0, 8, 6, 0};
    int P2[N] = { 5, 0, 2, 2, 3, 0, 0, 4, 0, 3};
    int P3[N] = { 5, 0, 1, 0, 0, 0, 0, 0, 0, 0};
    
    float hasilKurang , hasilTambah, hasilKali, hasilTurunan, hasilKali;
	
    hasilKurang = kurang(P1, P2, 1);
	printf("Hasil Pengurangan : %5.2f\n", hasilKurang);
	hasilTambah = tambah(P1, P2, 1);
	printf("Hasil Penjumlahan : %5.2f\n", hasilTambah); 
    hasilKali = kali(P1, P2, 1);
	printf("Hasil Pengurangan : %5.2f\n", hasilKali); 
        
}
