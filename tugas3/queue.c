#include <stdio.h>
#include <math.h>
#define MAX 100

typedef struct{
	int data[MAX];
	int front; //depan
	int rear; //belakang
	int count;
}Queue;

Queue antrian;
Queue *Q;

void Inisialisasi(){
	Q = &antrian;
	Q->front = -1;
	Q->rear = -1;
	Q->count = 0;
}

BOOL isEmpty() //cek kosong
{ 
	if(Q->front == -1){
		printf("Antrian Kosong\n");
		return true;
	}
	else{
		return false;
	}
}
BOOL isFull() ///cek full
{
	if(Q->rear == MAX - 1){
		printf("Antrian Penuh\n");
		return true;
	}
	else{
		return false;
	}
}

void Geser(int shift)
{
	int bil;
		
	for(int i = 1; i <= shift; i++){
		if(isEmpty() == true){
			break;
		}

		bil = Q->data[Q->front];
		int temp1 = 0, temp2 = 0;
		for (int j = (Q->count - 1); j >= Q->front; j-- )
		{
				
			if (j > (Q->rear - 1)){
				temp1 = Q->data[j];
				Q->data[j] = bil;
			}
			else{
				temp2 = Q->data[j];
				Q->data[j] = temp1;
				temp1 = temp2;
			}
		}			
	}
}
void ConvertDesimalKeBinary(int desimal)
{
	if (desimal > 0 ){
		int sisa, bagi, bil;
		bil = desimal;
			
		do{
			sisa = bil % 2; 
			bagi = bil / 2;
			bil = bagi;
			
			if(isFull() == true){
				break;
			}
			
			Q->rear++;
			Q->data[Q->rear] = sisa;
			Q->count++;

			if(Q->front == -1) Q->front = 0;
		}while(bil > 0);
	}
}


double ConvertBinaryKeDesimal()
{
	double hasil = 0.0;
	for(int i = 0 ;i <= (Q->count - 1); i++ ){
		hasil += (pow(2,i) * Q->data[i]);
	}
	return hasil;
}

void Tampil()
{
	for(int i = Q->rear; i >= Q->front; i-- ){
		printf("%d",Q->data[i]);
	}
	printf("\n");
}


void main(){
	int desimal, shift;
	double hasil;

	Inisialisasi();

	// printf("Masukkan Bilangan Desimal = ");
	// scanf("%d", &desimal);

	// printf("Masukkan Jumlah Shift = ");
	// scanf("%d", &shift);
	// printf("\n");
	// ConvertDesimalKeBinary(desimal);

	// printf("Hasil Konversi %d ke Biner :  ", desimal);
	// Tampil();
	// Geser(shift);

	// printf("Hasil Biner Setelah %d Shift : ", shift);
	// Tampil();
	// hasil = ConvertBinaryKeDesimal();

	// printf("\nKonversi Desimal %d setelah %d kali shift adalah %.0f\n", desimal, shift, hasil);
}

