#include <stdio.h>
#include <stdlib.h>
#define MAX 50

int queue_array[MAX];
int rear = - 1;
int front = - 1;
enqueue()
{
    int add_item;
    if (rear == MAX - 1)
    printf("Antrian sudah penuh\n");
    else
    {
        if (front == - 1)
        front = 0;
        printf("Element yang akan di masukkan : ");
        scanf("%d", &add_item);
        rear = rear + 1;
        queue_array[rear] = add_item;
    }
}
 
dequeue()
{
    if (front == - 1 || front > rear)
    {
        printf("Antian masih kosong \n");
        return ;
    }
    else
    {
        printf("Element yang akan di hapus : %d\n", queue_array[front]);
        front = front + 1;
    }
} 

tampil()
{
    int i;
    if (front == - 1)
        printf("Antrian kosong \n");
    else
    {
        printf("Antrian : \n");
        for (i = front; i <= rear; i++)
            printf("%d ", queue_array[i]);
        printf("\n");
    }
}

main()
{
    int pilihan;
    while (1)
    {
        printf("1. Enqueue \n");
        printf("2. Dequeue \n");
        printf("3. Tampil \n");
        printf("4. Selesai \n");
        printf("Pilihan anda : ");
        scanf("%d", &pilihan);
        switch (pilihan)
        {
            case 1:
            enqueue();
            break;
            case 2:
            dequeue();
            break;
            case 3:
            tampil();
            break;
            case 4:
            exit(1);
            default:
            exit(1);
        }
    }
}

