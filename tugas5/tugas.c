include <stdio.h>
#include <stdlib.h>

typedef int ItemType;
typedef struct simpul Node;
struct simpul {
	ItemType item;
	Node *next;
};
typedef struct {
	Node *TOS;
} Stack;

Node *baru;

void allocate_node(ItemType x)
{
	baru = (Node *)malloc(sizeof(Node));
	if (baru == NULL)
		printf ("Alokasi Linked List Gagal\n");
	else {
		baru->item = x;
		baru->next = NULL;
	}
}

void inisialisasi(Stack *s)
{
	s->TOS = NULL;
}

int kosong(Stack *s)
{
	return s->TOS == NULL;
}

void push(Stack *s)
{
	baru->next = s->TOS;
	s->TOS = baru;
}

ItemType pop(Stack *s)
{
	Node *tampung; // pointer temp untuk menunjuk data
	if (kosong(s))
		printf ("Data Stack Kosong\n\n");
	else {
		tampung = s->TOS;
		s->TOS = s->TOS->next;
		printf("Data %d Telah Dikeluarkan Dari Stack\n\n",tampung->item);
		return tampung->item;
		free(tampung); //bebas isi pointer tampung
		tampung = NULL; //pointer tampung menunujuk NULL
	}
	return 0;
}

void tampil(Stack *s)
{
	Stack temp; 
	temp = *s;
	printf ("\n Data Dalam Stack \n");
	while (temp.TOS != NULL)
	{
		printf("\n %d \n",temp.TOS->item);
		temp.TOS = temp.TOS->next;
	}
	printf("\n\n");
}


void main()
{
	int pil, data;
	char yn;
	Stack ahkir;

	inisialisasi(&ahkir);
	do 
	{
		printf("===============================\n");
		printf("Stack With Single Linked List\n");
		printf("===============================\n");
		printf("1. Push Data\n");
		printf("2. Pop Data\n");
		printf("3. Tampil Data Stack\n");
		printf("===================\n\n");
		printf("Masukkan Pilihan Anda : ");
		scanf("%d",&pil);

		if (pil == 1) {
			printf ("Masukkan Data : ");
			scanf("%d",&data);
			allocate_node(data);
			push(&ahkir);
		}
		else if (pil == 2) {
			pop(&ahkir);
		}
		else if (pil == 3) {
			tampil(&ahkir);
		}
		else 
			printf("Pilihan Anda Tidak Ada !!!!!\n\n");
		
		fflush(stdin);
		printf("Apakah Anda Ingin Coba Lagi [y/t] ? ");
		scanf("%c",&yn);
	} while(yn == 'Y' || yn == 'y');
}

