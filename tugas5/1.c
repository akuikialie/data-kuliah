#include <stdio.h>
#include <stdlib.h>

typedef int ItemType;
typedef struct simpul Node;
struct simpul {
	ItemType item;
	Node *next;
};
typedef struct {
	Node *TOS;
} Stack;

Node *baru;

void allocate_node(ItemType x)
{
	baru = (Node *)malloc(sizeof(Node));
	if (baru == NULL)
		printf ("Alokasi Linked List Gagal\n");
	else {
		baru->item = x;
		baru->next = NULL;
	}
}

void inisialisasi(Stack *s)
{
	s->TOS = NULL;
}

int empty(Stack *s)
{
	return s->TOS == NULL;
}

void push(Stack *s)
{
	baru->next = s->TOS;
	s->TOS = baru;
}

ItemType pop(Stack *s)
{
	Node *tmp; // pointer temp untuk menunjuk data
	if (empty(s))
		printf ("Data Stack Kosong\n\n");
	else {
		tmp = s->TOS;
		s->TOS = s->TOS->next;
		printf("Data %d sudah di keluarkan dari Stack\n\n",tmp->item);
		return tmp->item;
		free(tmp); //bebas isi pointer tmp
		tmp = NULL; //pointer tmp menunujuk NULL
	}
	return 0;
}

void view(Stack *s)
{
	Stack temp; 
	temp = *s;
	printf ("\n Data Dalam Stack \n");
	while (temp.TOS != NULL)
	{
		printf("\n %d \n",temp.TOS->item);
		temp.TOS = temp.TOS->next;
	}
	printf("\n\n");
}


void main()
{
	int pilihan, data;
	char isYes;
	Stack stacknya;

	inisialisasi(&stacknya);
	do 
	{
		printf("===============================\n");
		printf("Single Linked List\n");
		printf("===============================\n");
		printf("1. Push Data\n");
		printf("2. Pop Data\n");
		printf("3. Lihat Data Stack\n");
		printf("===================\n\n");
		printf("Masukkan Pilihan Anda : ");
		scanf("%d",&pilihan);

		if (pilihan == 1) {
			printf ("Masukkan data : ");
			scanf("%d",&data);
			allocate_node(data);
			push(&stacknya);
		}
		else if (pilihan == 2) {
			pop(&stacknya);
		}
		else if (pilihan == 3) {
			view(&stacknya);
		}
		else 
			printf("Tidak ada Pilihan!!!!!\n\n");
		
		fflush(stdin);
		printf("Coba lagi [y/t] ? ");
		scanf("%c",&isYes);
	} while(isYes == 'Y' || isYes == 'y');
}

