#include <stdio.h>

int cetak(int x) {
    if (x == 0) {
        x = 3;
        return x;
    } else {
        return cetak(x - 1) + 4;    
    }
}
    
main() {
    int n, i;    

    printf("masukkan N : ");
    scanf("%d", &n);
    
    printf("Hasil : ");
    for (i = 0 ; i < n; i++) {
        printf("%d ", cetak(i));
    }
    printf("\n\n");
}
