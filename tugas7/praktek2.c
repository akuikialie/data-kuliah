#include <stdio.h>

int cetak(int x) {
    if (x == 0 || x == 1) {
        x = 1;
        return x;
    } else {
        return 2 * cetak(x - 1) * cetak(x - 2);    
    }
}
    
main() {
    int n, i;    

    printf("masukkan N : ");
    scanf("%d", &n);
    
    printf("Hasil : ");
    for (i = 0 ; i < n; i++) {
        printf("%d ", cetak(i));
    }
    printf("\n\n");
}
