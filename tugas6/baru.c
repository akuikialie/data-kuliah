#include <stdio.h>
#include <stdlib.h>

typedef  int ItemType;
typedef struct simpul DNode;
struct simpul {

	ItemType item;
	DNode *next;
	DNode *prev;
};

typedef struct {
	DNode *front;
	DNode *rear;
	int count;
} Queue;

DNode *nodeBaru;

void allocate_node(ItemType x)
{
	nodeBaru = (DNode *)malloc(sizeof(DNode));
	if (nodeBaru == NULL)
		printf ("Alokasi Linked List\n\n");
	else {
		nodeBaru->item = x;
		nodeBaru->prev = NULL;
		nodeBaru->next = NULL;
	}
}

void Inisialisasi(Queue *q)
{
	q->front = NULL;
	q->rear = NULL;
	q->count = 0;
}

int Kosong (Queue *q)
{
	return q->rear==NULL;
}

void Enqueue(Queue *q, ItemType x)
{
	if(Kosong(q)) { //bila queue masi Kosong
		q->front = nodeBaru;
		q->rear = nodeBaru;
		printf ("Data %d berhasil masuk\n\n",x);
	} else {
		nodeBaru->prev = q->rear;
		q->rear->next = nodeBaru;
		q->rear = nodeBaru;
		printf ("Data %d berhasil masuk\n\n",x);
	}
	q->count++;

}

ItemType Dequeue(Queue *q)
{
	DNode *tmp;
	if(Kosong(q)) {
		printf ("Data Masi Kosong\n\n");
	} else {
		tmp = q->front;
		if(q->front == q->rear) {
			q->front = NULL;
			q->rear = NULL;
			printf("Data %d Berhasil Di Keluarkan\n\n",tmp->item);
		} else {
			q->front = q->front->next;
			q->front->prev = NULL;
			printf("Data %d Berhasil Di Keluarkan\n\n",tmp->item);
		}
		return tmp->item;
		free(tmp);
		tmp = NULL;
		q->count--;

	}
	return 0;
}

void view_data(Queue *q)
{
	Queue tmp;
	tmp = *q;
	printf ("\nData Dalam Queue \n\n");
	printf ("Front ");
	while (tmp.front != NULL)
	{
		printf("%d ",tmp.front->item);
		tmp.front = tmp.front->next;
	}
	printf ("Rear ");
	printf("\n\n");
}

void main()
{
	Queue simpul;
	ItemType data;
	int pilihan;
	Inisialisasi(&simpul);
	char yn;
	do {
		printf ("=====================================\n");
		printf ("Queue Double Linked List\n");
		printf ("=====================================\n");
		printf ("1. Enqueue \n");
		printf ("2. Dequeue \n");
		printf ("3. View Data \n");
		printf ("=====================================\n");
		printf ("Pilihan Anda : ");
		scanf ("%d", &pilihan);
		if (pilihan == 1) {
			printf ("Masukkan Data : ");
			scanf ("%d", &data);
			allocate_node(data);
			Enqueue(&simpul, data);
		} else if (pilihan == 2) {
			Dequeue(&simpul);
		} else if (pilihan == 3) {
			view_data(&simpul);
		} else
			printf ("Pilihan anda tidak tersedia !!!!!\n\n");
		fflush(stdin);
		printf ("Coba lagi [y/t] ?");
		scanf ("%c",&yn);
	} while (yn == 'y' || yn == 'Y');

}
