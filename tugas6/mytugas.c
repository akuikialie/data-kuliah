#include <stdio.h>
#include <stdlib.h>

typedef  int ItemType;
typedef struct simpul DNode;
struct simpul {

    ItemType item;
    DNode *next;
    DNode *prev;
};
typedef struct {
    DNode *front;
    DNode *rear;
    int count;
} Queue;

DNode *baru;

void allocate_node(ItemType x)
{
    baru = (DNode *)malloc(sizeof(DNode));
    if (baru == NULL) {
        printf ("Alokasi\n\n");
    } else {
        baru->item = x;
        baru->prev = NULL;
        baru->next = NULL;
    }
}

void inisialisasi(Queue *q)
{
    q->front = NULL;
    q->rear = NULL;
    q->count = 0;
}

int kosong (Queue *q)
{
    return q->rear==NULL;
}

void enqueue(Queue *q, ItemType x)
{
    if (kosong(q)) {
        q->front = baru;
        q->rear = baru;
        printf ("Data %d Berhasil Di Masukkann\n\n",x);
    } else {
        baru->prev = q->rear;
        q->rear->next = baru;
        q->rear = baru;
        printf ("Data %d Berhasil Di Masukkann\n\n",x);
    }
    q->count++;

}

ItemType dequeue(Queue *q)
{
    DNode *temp;
    if (kosong(q)) {
        printf ("Data Masi Kosong\n\n");
    } else {
        temp = q->front;
        if (q->front == q->rear) {
            q->front = NULL;
            q->rear = NULL;
            printf("Data %d Berhasil Di Keluarkan\n\n",temp->item);
        } else {
            q->front = q->front->next;
            q->front->prev = NULL;
            printf("Data %d Berhasil Di Keluarkan\n\n",temp->item);
        }
        return temp->item;
        free(temp);
        temp = NULL;
        q->count--;

    }
    return 0;
}

void tampil_data(Queue *q)
{
    Queue temp;
    temp = *q;
    printf ("\nData \n\n");
    printf ("Front ");
    while (temp.front != NULL)
    {
        printf("%d ",temp.front->item);
        temp.front = temp.front->next;
    }
    printf ("Rear ");
    printf("\n\n");
}

void main()
{
    Queue simpul;
    ItemType data;
    int pilihan;
    inisialisasi(&simpul);
    char ulang;
    while (ulang == 'y'){
        printf ("=====================================\n");
        printf ("Queue Double Linked List\n");
        printf ("=====================================\n");
        printf ("1. Enqueue \n");
        printf ("2. Dequeue \n");
        printf ("3. View Data \n");
        printf ("=====================================\n");
        printf ("Pilihan Anda : ");
        scanf ("%d", &pil);
        if (pil == 1) {
            printf ("Input Data : ");
            scanf ("%d", &data);
            allocate_node(data);
            enqueue(&simpul, data);
        } else if (pil == 2) {
            dequeue(&simpul);
        } else if (pil == 3) {
            tampil_data(&simpul);
        } else
            printf ("Pilihan anda salah!\n\n");
        fflush(stdin);
        printf ("Ulang [y/t] ?\n");
        scanf ("%c", &ulang);
    }
}


