#include <stdio.h>
#include <stdlib.h>

typedef struct simpul Node; 
struct simpul {
	int nrp; 
	char nama[20];
	int kelas;
	Node *next; 
};

Node *head=NULL, *baru; 

void allocate_node(int x, char n[20],int k)
{ 
	baru = (Node *)malloc(sizeof(Node));

	if (baru == NULL) {
		printf ("alokasi gagal\n");
	}
	else {
		baru->nrp = x;
		baru->nama[20] = strcpy(baru->nama, n);
		baru->kelas = k;
		baru->next= NULL;
	}
}

void tampil()
{
	Node *t = head;
	printf("\nHasil Data \n\n");
	while(t != NULL) {
		printf("++++++++++++++++++++++++++++++++++\n");
		printf("+   nrp  +     nama     +  kelas +\n");
		printf("++++++++++++++++++++++++++++++++++\n");
		printf("+   %d   +      %s      +  %d    +\n",t->nrp, t->nama, t->kelas);
		t = t->next;
		printf("++++++++++++++++++++++++++++++++++\n");
	}
	printf("\n");
}
void sisipAwal()
{
	baru->next = head;
	head = baru;
}

void sisipAkhir()
{
	Node *ahkir = head;
	while(ahkir->next != NULL)
		ahkir = ahkir->next;
	ahkir->next = baru;
}
void sisip_sebelum(int x, char n[20],int k)
{
	Node *before = head;
	while (before->next->nrp != x)
		before = before->next;
	baru->next = before->next;
	before->next = baru;
}
void free_node(Node *p) // untuk hapus node
{
	free(p);
	p = NULL;
}

void hapusAwal()
{
	Node *hapus=head;
	head = hapus->next;
	free_node(hapus);
}

void hapusTengah(int x)
{
	Node *hapus, *before;
	hapus = before = head;
	while(hapus->nrp != x) {
		before = hapus;
		hapus = hapus->next;
	}
	before->next= hapus->next;
	free_node(hapus);
}

void sisip() //prosedur utama sisip
{
	Node *p;
	if(head==NULL)
		head = baru;
	else if (head->nrp > baru->nrp)
		sisipAwal();
	else {
		p = head;
		while(p != NULL && p->nrp < baru->nrp)
			p=p->next;
		if (p == NULL)
			sisipAkhir();
		else if (p->nrp == baru->nrp)
			printf ("Ada data yang sama, tidak dapat disisipkan\n");
		else
			sisip_sebelum(p->nrp,p->nama,p->kelas);
	}
}

void hapus(int x) //prosedur utama hapus
{
	Node *hapus = head;
	if (head->nrp == x)
		hapusAwal();
	else {
		while(hapus != NULL && hapus->nrp != x)
			hapus=hapus->next;
		if(hapus == NULL)
			printf("Data Yang Akan Di Hapus Tidak Ada\n");
		else
			hapusTengah(hapus->nrp);
	}
}
void main()
{	
	int a,b,pil;
	char c[20], yn;
	head=baru;
	do 
	{
		printf("Menu Linked List\n");
		printf("1. Sisip Data\n");
		printf("2. Hapus Data\n");
		printf("\n\n");
		printf("Pilih menu : ");
		scanf("%d",&pil);
		if (pil == 1) {
			printf("Masukkan nrp : ");
			scanf("%d",&a);
			printf("Masukkan nama : ");
			scanf("%s",&c); 
			printf("Masukkan kelas : ");
			scanf("%d",&b);
			allocate_node(a,c,b);
			sisip();
		}
		else if (pil == 2) {
			printf("masukkan nrp yang akan di hapus  : ");
			scanf("%d",&a);
			allocate_node(a,c,b);
			hapus(a);
		}
		else{
			printf("nrp tidak ada !!!	\n\n");
		}
		tampil();
		fflush(stdin);
		printf("Ingin ulang kembali [y/t] ? : ");
		scanf("%c",&yn);	
	} while(yn == 'y' || yn =='Y');
	

}