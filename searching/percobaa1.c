#include <stdio.h>
#include <stdlib.h>
#define MAX 10
int Data[MAX];
int SequentialSearch(int x)
{
    int i = 0;
    int ketemu = 0;
    while ((ketemu == 0) && (i < MAX)){
        if(Data[i] == x) {
            ketemu = 1;
        } else {
            i++;
        }
    }
    if (ketemu) {
        return i;
    } else {
        return -1;
    }
}

void main()
{
    int i,key, indeks;
    //pembangkit bilangan random
    srand(time(NULL));
    //membangkitkan bilangan integer random
    printf("\nDATA : \n");
    for (i = 0; i < MAX; i++)
    {
        Data[i] = rand()%100+1;
        printf(" %d[%d] \n", Data[i], i);
    }

    int k;
    printf("\nKunci : ");
    scanf("%d", &k);

    int ketemu = SequentialSearch(k);
    if(ketemu > 0)
        printf("Data %d telah ditemukan pada posisi %d", k, ketemu);
    else
        printf("Data %d tidak ditemukan.", k);
    printf("\n");
}

